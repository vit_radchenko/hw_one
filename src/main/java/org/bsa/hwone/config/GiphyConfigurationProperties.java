package org.bsa.hwone.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Setter
@Getter
@ConfigurationProperties(prefix = "giphy")
public class GiphyConfigurationProperties {
    private String url;
    private String apiKey;
}
