package org.bsa.hwone.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class StarterHookConfiguration {

    @Value("cache")
    private String packageName;

    @Bean
    CommandLineRunner setUp() {
        return (args) -> {
            var basePath = System.getProperty("user.dir");
            Files.createDirectories(Paths.get(basePath, packageName));
            Files.createDirectories(Paths.get(basePath, "users"));
        };
    }
}
