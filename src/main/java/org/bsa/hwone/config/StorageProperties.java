package org.bsa.hwone.config;

import org.springframework.stereotype.Component;

import java.nio.file.Paths;

@Component
public class StorageProperties {
    static final String BASE_PATH = System.getProperty("user.dir");

    public String getBasePath() {
        return BASE_PATH;
    }

    public String getCachePackage() {
        return Paths.get(BASE_PATH, "cache").toString();
    }

    public String getUserPackage() {
        return Paths.get(BASE_PATH, "users").toString();
    }

    public String getUserPackage(String id) {
        return Paths.get(BASE_PATH, "users", id).toString();
    }
}
