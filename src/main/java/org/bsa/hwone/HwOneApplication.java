package org.bsa.hwone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class HwOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(HwOneApplication.class, args);
	}

}
