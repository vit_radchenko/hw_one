package org.bsa.hwone.utils;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Component
public class Randomizer {

    public <T> T randomElement(List<T> list) {
        return list.get(new Random().nextInt(list.size()));
    }
}
