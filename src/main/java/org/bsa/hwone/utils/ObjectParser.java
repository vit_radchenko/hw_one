package org.bsa.hwone.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ObjectParser {

    private final ObjectMapper mapper;

    @Autowired
    public ObjectParser(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public String parseToJson(Object o) {
        try {
            return mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Failed to parse object");
        }
    }
}
