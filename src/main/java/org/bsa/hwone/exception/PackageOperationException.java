package org.bsa.hwone.exception;

public class PackageOperationException extends RuntimeException {

    public PackageOperationException() {
    }

    public PackageOperationException(String message) {
        super(message);
    }

    public PackageOperationException(String message, Throwable cause) {
        super(message, cause);
    }
}
