package org.bsa.hwone.exception;

public class NotFoundGifException extends RuntimeException {
    public NotFoundGifException() {
        super();
    }

    public NotFoundGifException(String message) {
        super(message);
    }
}
