package org.bsa.hwone.exception;

public class FailSaveGifException extends RuntimeException {
    public FailSaveGifException() {
        super();
    }

    public FailSaveGifException(String message) {
        super(message);
    }

    public FailSaveGifException(String message, Throwable cause) {
        super(message, cause);
    }
}
