package org.bsa.hwone.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Map;

@ControllerAdvice
public class Handler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> handleBrokenUserDataException(IllegalArgumentException ex) {
        return ResponseEntity
                .unprocessableEntity()
                .body(
                        Map.of(
                                "error", ex.getMessage() == null ? "Unprocessable request parameters" : ex.getMessage()
                        )
                );
    }

    @ExceptionHandler(FailSaveGifException.class)
    public ResponseEntity<Object> handleSaveGifException(IllegalArgumentException ex) {
        return ResponseEntity
                .unprocessableEntity()
                .body(
                        Map.of(
                                "error", ex.getMessage() == null ? "Failed to save gif" : ex.getMessage()
                        )
                );
    }

    @ExceptionHandler(NotFoundGifException.class)
    public ResponseEntity<Object> handleNotFoundGifException(NotFoundGifException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(HeaderMissedException.class)
    public ResponseEntity<Object> handleMissedHeader(HeaderMissedException ex) {
        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}
