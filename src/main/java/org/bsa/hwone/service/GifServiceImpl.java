package org.bsa.hwone.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Builder;
import lombok.Getter;
import org.bsa.hwone.config.GiphyConfigurationProperties;
import org.bsa.hwone.exception.FailSaveGifException;
import org.bsa.hwone.model.GifModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class GifServiceImpl implements GifService {
    private static final Logger logger = LoggerFactory.getLogger(GifServiceImpl.class);

    @Value("cache")
    private String packageName;

    private final HttpClient httpClient;
    private final GiphyConfigurationProperties giphyProperties;
    private final ObjectMapper mapper;

    @Autowired
    public GifServiceImpl(HttpClient httpClient, GiphyConfigurationProperties giphyProperties, ObjectMapper mapper) {
        this.httpClient = httpClient;
        this.giphyProperties = giphyProperties;
        this.mapper = mapper;
    }

    @Override
    public GifModel getGig(String query) {
        try {
            GifEntry gitEntry = getGifUrl(query);
            var response = httpClient.send(buildGetGifImageRequest(gitEntry.getUrl()), HttpResponse.BodyHandlers.ofByteArray());
            return GifModel.builder()
                    .id(gitEntry.getId())
                    .data(response.body())
                    .build();
        } catch (IOException | InterruptedException e) {
            logger.error(e.getMessage(), e);
            throw new FailSaveGifException(e.getMessage(), e);
        }
    }

    private GifEntry getGifUrl(String query) throws IOException, InterruptedException {
        var response = httpClient.send(buildGetGifUrlRequest(query), HttpResponse.BodyHandlers.ofString());
        var jsonNode = mapper.readValue(response.body(), ObjectNode.class);

        var id = jsonNode.findValues("data").get(0).findValue("id").asText();
        var url = jsonNode.findValues("data").get(0).findValue("original").findValue("url").asText();

        return GifEntry.builder().id(id)
                .url(url)
                .build();
    }

    private HttpRequest buildGetGifImageRequest(String url) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(url))
                .GET()
                .build();
    }

    private HttpRequest buildGetGifUrlRequest(String query) {
        return HttpRequest
                .newBuilder()
                .uri(URI.create(generateUrl(query)))
                .GET()
                .build();
    }

    private String generateUrl(String tag) {
        return String.format("%s?api_key=%s&tag=%s", giphyProperties.getUrl(), giphyProperties.getApiKey(), tag);
    }

    @Getter
    @Builder
    private static class GifEntry {
        private String id;
        private String url;
    }
}
