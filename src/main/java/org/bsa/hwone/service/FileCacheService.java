package org.bsa.hwone.service;

import org.bsa.hwone.exception.NotFoundGifException;
import org.bsa.hwone.model.QueryGifsModel;
import org.bsa.hwone.repository.FileRepository;
import org.bsa.hwone.utils.ObjectParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileCacheService {
    private final FileRepository fileRepository;
    private final GifService gifService;
    private final ObjectParser parser;

    @Autowired
    public FileCacheService(FileRepository fileRepository, GifService gifService, ObjectParser parser) {
        this.fileRepository = fileRepository;
        this.gifService = gifService;
        this.parser = parser;
    }

    public void cleanCache() {
        fileRepository.cleanCache();
    }

    public String generate(String query) {
        var gifModel = gifService.getGig(query);
        fileRepository.saveInCache(query, gifModel);

        return parser.parseToJson(fileRepository.findInCache(query));
    }

    public String findInCache(String query) {
        if (query == null || query.isBlank()) {
            return parser.parseToJson(fileRepository.getCacheStructure());
        }

        List<String> list = fileRepository.findInCache(query);
        if (list.isEmpty()) {
            throw new NotFoundGifException("Failed to find images by query: " + query);
        }

        return parser.parseToJson(QueryGifsModel.of(query, list));
    }
}
