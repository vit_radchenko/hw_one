package org.bsa.hwone.service;

import org.bsa.hwone.config.StorageProperties;
import org.bsa.hwone.exception.PackageOperationException;
import org.bsa.hwone.model.HistoryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.String.format;

@Component
public class HistoryService {
    private final StorageProperties storage;

    private static final String HISTORY_FILE_NAME = "history.csv";

    @Autowired
    public HistoryService(StorageProperties storage) {
        this.storage = storage;
    }

    public void updateHistory(String id, HistoryDto historyDto) {
        Path historyPath = Path.of(storage.getUserPackage(id), HISTORY_FILE_NAME);

        try {
            if (!historyPath.toFile().exists()) {
                Files.createFile(historyPath);
            }

            String historyAsString = convertToCsvString(historyDto) + "\r\n";
            Files.write(historyPath, historyAsString.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            throw new PackageOperationException(String.format("Failed to update history for id: %s", id));
        }
    }

    public List<HistoryDto> readHistory(String id) {
        Path historyPath = Path.of(storage.getUserPackage(id), HISTORY_FILE_NAME);

        try {
            List<String> allLines = Files.readAllLines(historyPath);

            return allLines
                    .stream()
                    .filter(line -> !line.isBlank())
                    .map(this::convertToDto)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new PackageOperationException(String.format("Failed to read history for id: %s", id));
        }
    }


    public void cleanHistory(String id) {
        Path historyPath = Path.of(storage.getUserPackage(id), HISTORY_FILE_NAME);
        try {
            Files.write(historyPath, "".getBytes());
        } catch (IOException e) {
            throw new PackageOperationException(format("Failed to clean history for id: %s", id));
        }
    }

    private HistoryDto convertToDto(String line) {
        String[] arr = line.split(",");
        return HistoryDto.builder()
                .date(arr[0])
                .query(arr[1])
                .gif(arr[2])
                .build();
    }

    private String convertToCsvString(HistoryDto dto) {
        return Stream.of(dto.getDate(), dto.getQuery(), dto.getGif())
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }

    private String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }
}
