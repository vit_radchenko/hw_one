package org.bsa.hwone.service;

import org.bsa.hwone.exception.NotFoundGifException;
import org.bsa.hwone.model.GenerateGifRequest;
import org.bsa.hwone.model.HistoryDto;
import org.bsa.hwone.model.QueryGifsModel;
import org.bsa.hwone.repository.FileRepository;
import org.bsa.hwone.repository.RuntimeCacheRepository;
import org.bsa.hwone.utils.ObjectParser;
import org.bsa.hwone.utils.Randomizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    private final FileRepository fileRepository;
    private final GifService gifService;
    private final ObjectParser parser;
    private final Randomizer randomizer;
    private final HistoryService historyService;
    private final RuntimeCacheRepository runtimeCacheRepository;

    @Autowired
    public UserService(FileRepository fileRepository, GifService gifService, ObjectParser parser, Randomizer randomizer, HistoryService historyService, RuntimeCacheRepository runtimeCacheRepository) {
        this.fileRepository = fileRepository;
        this.gifService = gifService;
        this.parser = parser;
        this.randomizer = randomizer;
        this.historyService = historyService;
        this.runtimeCacheRepository = runtimeCacheRepository;
    }

    public String userGenerate(String id, GenerateGifRequest request) {
        final String force = request.getForce();
        final String query = request.getQuery();
        logger.info("Generate User Request: force {}, model: {}", force, request);

        if ("true".equalsIgnoreCase(force)) {
            var gifModel = gifService.getGig(query);

            var opt = fileRepository.findInUser(id, query, gifModel.getId());
            return opt.orElseGet(() -> fileRepository.saveToUser(id, query, gifModel));
        }

        List<String> gifsInCache = fileRepository.findInCache(query);
        if (!gifsInCache.isEmpty()) {
            var pathToGif = randomizer.randomElement(gifsInCache);
            String savedGifPath = fileRepository.copyToUser(pathToGif, id, query);
            runtimeCacheRepository.updateCache(id, query, savedGifPath);

            historyService.updateHistory(id, HistoryDto.now(query, pathToGif));
            return pathToGif;
        }


        var gifModel = gifService.getGig(query);
        String savedPath = fileRepository.saveInCache(query, gifModel);
        String savedGifPath = fileRepository.copyToUser(savedPath, id, query);
        runtimeCacheRepository.updateCache(id, query, savedGifPath);

        historyService.updateHistory(id, HistoryDto.now(query, savedPath));
        return savedPath;
    }

    public String findInUserCache(String id, String query, String force) {
        logger.info("Find User Request: force {}, id: {}, query: {}", force, id, query);

        if (force != null) {
            Optional<String> opt = runtimeCacheRepository.find(id, query);
            if (opt.isPresent()) {
                String path = opt.get();
                historyService.updateHistory(id, HistoryDto.now(query, path));
                return path;
            }
        }

        //go to user cache
        Optional<String> opt = fileRepository
                .findInUser(id, query);

        if (opt.isPresent()) {
            String path = opt.get();
            historyService.updateHistory(id, HistoryDto.now(query, path));
            return path;
        }

        historyService.updateHistory(id, HistoryDto.now(query));
        throw new NotFoundGifException(String.format("Gif with query: %s not found for id/path: %s", query, id));
    }

    public String findAllInUserCache(String id) {
        List<QueryGifsModel> list = fileRepository.findInUser(id);
        if (list.isEmpty()) {
            throw new NotFoundGifException();
        }

        return parser.parseToJson(list);
    }

    public void clean(String id) {
        runtimeCacheRepository.cleanCache(id);
        fileRepository.cleanForUser(id);
    }

    public String getAllGifs() {
        return parser.parseToJson(fileRepository.getAllFiles());
    }

    public String findHistory(String id) {
        return parser.parseToJson(historyService.readHistory(id));
    }

    public void cleanHistory(String id) {
        historyService.cleanHistory(id);
    }
}
