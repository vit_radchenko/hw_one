package org.bsa.hwone.controller;

import org.bsa.hwone.model.GenerateGifRequest;
import org.bsa.hwone.service.FileCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Controller
@RequestMapping(path = "/cache")
public class CacheController {
    private static final Logger logger = LoggerFactory.getLogger(CacheController.class);

    private final FileCacheService fileCacheService;

    @Autowired
    public CacheController(FileCacheService fileCacheService) {
        this.fileCacheService = fileCacheService;
    }

    @GetMapping
    public ResponseEntity<String> find(@RequestParam(required = false) @Min(1) @Max(100) String query) {
        logger.info("GET to '/cache' request with request param, query: {}", query);
        return new ResponseEntity<>(fileCacheService.findInCache(query), HttpStatus.OK);
    }

    @PostMapping(path = "/generate")
    public ResponseEntity<String> generate(@Validated @RequestBody GenerateGifRequest request) {
        logger.info("POST to '/cache/generate' request with request: {}", request);
        return new ResponseEntity<>(fileCacheService.generate(request.getQuery()), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<Void> clean() {
        logger.info("DELETE to '/cache' request");
        fileCacheService.cleanCache();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
