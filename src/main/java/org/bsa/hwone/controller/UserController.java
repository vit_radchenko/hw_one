package org.bsa.hwone.controller;

import org.bsa.hwone.model.GenerateGifRequest;
import org.bsa.hwone.repository.RuntimeCacheRepository;
import org.bsa.hwone.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;
    private final RuntimeCacheRepository cacheService;

    @Autowired
    public UserController(UserService userService, RuntimeCacheRepository cacheService) {
        this.userService = userService;
        this.cacheService = cacheService;
    }

    @GetMapping(path = "/{id}/search")
    public ResponseEntity<String> search(@RequestParam @Min(1) @Max(100) String id,
                                         @RequestParam @Min(1) @Max(100) String query,
                                         @RequestParam(required = false) @Min(1) @Max(100) String force) {
        logger.info("GET request to '/user/{id}/search' where id: {}, query: {}, force: {}", id, query, force);
        return new ResponseEntity<>(userService.findInUserCache(id, query, force), HttpStatus.OK);
    }

    @PostMapping(path = "/{id}/generate")
    public ResponseEntity<String> generate(@PathVariable String id, @Validated @RequestBody GenerateGifRequest request) {
        logger.info("POST request to '/user/{id}/generate' where id: {},request: {}", id, request);
        return new ResponseEntity<>(userService.userGenerate(id, request), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/all")
    public ResponseEntity<String> getAll(@PathVariable String id) {
        logger.info("GET request to '/user/{id}/all' where id: {}", id);
        return new ResponseEntity<>(userService.findAllInUserCache(id), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/history")
    public ResponseEntity<String> getHistory(@PathVariable String id) {
        logger.info("GET request to '/user/{id}/history' where id: {}", id);
        return new ResponseEntity<>(userService.findHistory(id), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}/history/clean")
    public ResponseEntity<Void> cleanHistory(@PathVariable String id) {
        logger.info("DELETE request to '/user/{id}/history/clean' where id: {}", id);
        userService.cleanHistory(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}/reset")
    public ResponseEntity<Void> reset(@PathVariable String id, @RequestParam(required = false) String query) {
        logger.info("DELETE request to '/user/{id}/reset' where id: {}, query: {}", id, query);
        cacheService.cleanCache(id, query);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}/clean")
    public ResponseEntity<Void> clean(@PathVariable String id) {
        logger.info("DELETE request to '/user/{id}/clean' where id: {}", id);
        userService.clean(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
