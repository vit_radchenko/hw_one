package org.bsa.hwone.repository;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.bsa.hwone.config.StorageProperties;
import org.bsa.hwone.exception.FailSaveGifException;
import org.bsa.hwone.model.GifModel;
import org.bsa.hwone.model.QueryGifsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileRepositoryImpl implements FileRepository {
    private static final Logger logger = LoggerFactory.getLogger(FileRepositoryImpl.class);
    private final StorageProperties storage;

    @Autowired
    public FileRepositoryImpl(StorageProperties storage) {
        this.storage = storage;
    }

    public List<QueryGifsModel> getCacheStructure() {
        return filesStream(storage.getCachePackage())
                .filter(File::isDirectory)
                .map(this::mapDirectoryStructureToModel)
                .collect(Collectors.toList());
    }

    public List<String> getAllFiles() {
        return filesStream(storage.getCachePackage())
                .flatMap(dir -> Stream.of(Objects.requireNonNull(dir.listFiles())))
                .filter(File::isFile)
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }

    public List<String> findInCache(String query) {
        return filesStream(storage.getCachePackage())
                .filter(File::isDirectory)
                .filter(f -> f.getName().equalsIgnoreCase(query))
                .flatMap(dir -> Stream.of(Objects.requireNonNull(dir.listFiles())))
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());
    }

    public List<QueryGifsModel> findInUser(String id) {
        String userPath = storage.getUserPackage(id);

        if (!Path.of(userPath).toFile().exists()) {
            return new ArrayList<>();
        }

        return filesStream(userPath)
                .filter(File::isDirectory)
                .map(this::mapDirectoryStructureToModel)
                .collect(Collectors.toList());
    }

    public Optional<String> findInUser(String id, String query) {
        String userPath = storage.getUserPackage(id);

        if (!Path.of(userPath).toFile().exists()) {
            return Optional.empty();
        }

        return filesStream(userPath)
                .filter(File::isDirectory)
                .filter(dir -> dir.getName().equalsIgnoreCase(query))
                .flatMap(dir -> Stream.of(Objects.requireNonNull(dir.listFiles())))
                .map(File::getAbsolutePath)
                .findAny();
    }

    public String saveInCache(String query, GifModel gifModel) {
        Path packagePath = Paths.get(storage.getCachePackage(), query);
        Path filePath = Paths.get(storage.getCachePackage(), query, gifModel.getId() + ".gif");
        try {
            Files.createDirectories(packagePath);
            return Files.write(filePath, gifModel.getData()).toString();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new FailSaveGifException(e.getMessage());
        }
    }

    public String copyToUser(String source, String id, String query) {
        Path srcPath = Path.of(source);
        Path destPackagePath = Path.of(storage.getUserPackage(id), query);
        Path destFilePath = Path.of(storage.getUserPackage(id), query, srcPath.toFile().getName());

        try {
            Files.createDirectories(destPackagePath);
            Files.copy(srcPath, destFilePath, StandardCopyOption.REPLACE_EXISTING);
            return destFilePath.toString();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new FailSaveGifException("Failed to copy gif for id: " + id, e);
        }
    }

    public Optional<String> findInUser(String id, String query, String name) {
        String userPath = storage.getUserPackage(id);
        if (!Path.of(userPath).toFile().exists()) {
            return Optional.empty();
        }

        return filesStream(userPath)
                .filter(File::isDirectory)
                .filter(dir -> dir.getName().equalsIgnoreCase(query))
                .flatMap(dir -> Stream.of(Objects.requireNonNull(dir.listFiles())))
                .filter(file -> file.getName().contains(name))
                .map(File::getAbsolutePath)
                .findAny();
    }

    public String saveToUser(String id, String query, GifModel gifModel) {
        Path path = Paths.get(storage.getUserPackage(id), query, gifModel.getId() + ".gif");
        try {
            Files.createDirectories(Paths.get(storage.getUserPackage(id), query));
            return Files.write(path, gifModel.getData()).toString();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new FailSaveGifException(e.getMessage());
        }
    }

    public void cleanForUser(String id) {
        Path path = Paths.get(storage.getUserPackage(id));
        try {
            FileUtils.cleanDirectory(path.toFile());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public void cleanCache() {
        Path path = Paths.get(storage.getCachePackage());
        try {
            FileUtils.cleanDirectory(path.toFile());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private Stream<File> filesStream(String path) {
        return Stream.of(Objects.requireNonNull(Path.of(path).toFile().listFiles()));
    }

    private QueryGifsModel mapDirectoryStructureToModel(File dir) {
        Collection<String> pathes = Stream.of(Objects.requireNonNull(dir.listFiles()))
                .filter(File::isFile)
                .map(File::getAbsolutePath)
                .collect(Collectors.toList());

        var model = QueryGifsModel.of(dir.getName());
        model.addGifPath(pathes);
        return model;
    }
}
