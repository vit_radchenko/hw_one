package org.bsa.hwone.repository;

import org.bsa.hwone.model.QueryGifsModel;
import org.bsa.hwone.utils.Randomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class RuntimeCacheRepositoryImpl implements RuntimeCacheRepository {
    private Map<String, Set<QueryGifsModel>> cache = new HashMap<>();

    private final Randomizer randomizer;

    @Autowired
    public RuntimeCacheRepositoryImpl(Randomizer randomizer) {
        this.randomizer = randomizer;
    }

    public Optional<String> find(String id, String query) {
        if (!cache.containsKey(id)) {
            return Optional.empty();
        }

        Optional<QueryGifsModel> foundGifModel = cache.get(id)
                .stream()
                .filter(queryGifsModel -> queryGifsModel.getQuery().equalsIgnoreCase(query))
                .findAny();

        if (foundGifModel.isPresent()) {
            List<String> list = new ArrayList<>(foundGifModel.get().getPaths());
            String path = randomizer.randomElement(list);
            return Optional.of(path);
        }

        return Optional.empty();
    }

    public void updateCache(String id, String query, String pathToAdd) {
        if (cache.containsKey(id)) {
            Optional<QueryGifsModel> opt = cache.get(id)
                    .stream()
                    .filter(model -> model.getQuery().equals(query))
                    .findFirst();

            if (opt.isPresent()) {
                opt.get().addGifPath(pathToAdd);
            } else {
                cache.get(id).add(QueryGifsModel.of(query, pathToAdd));
            }
        } else {
            var model = QueryGifsModel.of(query, pathToAdd);
            Set<QueryGifsModel> set = new HashSet<>();
            set.add(model);
            cache.put(id, set);
        }
    }

    public void cleanCache(String id) {
        cleanCache(id, null);
    }

    public void cleanCache(String id, String query) {
        if (!cache.containsKey(id)) {
            return;
        }

        if (query == null) {
            cache.remove(id);
            return;
        }

        var userSet = cache.get(id);
        var iterator = userSet.iterator();
        while (iterator.hasNext()) {
            var model = iterator.next();
            if (model.getQuery().equalsIgnoreCase(query)) {
                iterator.remove();
                break;
            }
        }
    }
}