package org.bsa.hwone.repository;

import org.bsa.hwone.model.GifModel;
import org.bsa.hwone.model.QueryGifsModel;

import java.util.List;
import java.util.Optional;

public interface FileRepository {
    //split by cache
    List<String> findInCache(String query);

    String saveInCache(String query, GifModel gifModel);

    void cleanCache();

    List<QueryGifsModel> getCacheStructure();

    List<String> getAllFiles();

    //split by user
    Optional<String> findInUser(String id, String query, String name);

    List<QueryGifsModel> findInUser(String id);

    Optional<String> findInUser(String id, String query);

    String copyToUser(String source, String id, String query);

    String saveToUser(String id, String query, GifModel gifModel);

    void cleanForUser(String id);
}
