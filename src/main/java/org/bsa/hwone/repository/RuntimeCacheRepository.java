package org.bsa.hwone.repository;

import java.util.Optional;

public interface RuntimeCacheRepository {
    void updateCache(String id, String query, String pathToAdd);

    void cleanCache(String id);

    void cleanCache(String id, String query);

    Optional<String> find(String id, String query);
}
