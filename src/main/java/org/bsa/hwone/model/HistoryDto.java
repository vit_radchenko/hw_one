package org.bsa.hwone.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoryDto {
    private String date;
    private String query;
    private String gif;

    public static HistoryDto now(String query) {
        return now(query, "");
    }

    public static HistoryDto now(String query, String gifPath) {
        String date = LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return builder()
                .date(date)
                .query(query)
                .gif(gifPath)
                .build();
    }
}
