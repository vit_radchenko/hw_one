package org.bsa.hwone.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(of = "query")
public class QueryGifsModel {
    @Getter
    private String query;
    @Getter
    @JsonProperty("gifs")
    private Set<String> paths = new HashSet<>();

    private QueryGifsModel(String query) {
        this.query = query;
    }

    private QueryGifsModel(String query, String path) {
        this.query = query;
        paths.add(path);
    }

    public void addGifPath(String path) {
        paths.add(path);
    }

    public void addGifPath(Collection<String> paths) {
        this.paths.addAll(paths);
    }

    public static QueryGifsModel of(String query) {
        return new QueryGifsModel(query);
    }

    public static QueryGifsModel of(String query, String gifPath) {
        return new QueryGifsModel(query, gifPath);
    }

    public static QueryGifsModel of(String query, Collection<String> gifPaths) {
        var model = new QueryGifsModel(query);
        model.addGifPath(gifPaths);
        return model;
    }
}
