package org.bsa.hwone.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(of = "id")
public class GifModel {
    private final String id;
    private final byte[] data;
}
