package org.bsa.hwone.interceptor;

import org.bsa.hwone.exception.HeaderMissedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RequestHeaderInterceptor extends HandlerInterceptorAdapter {

    @Value("enable-interceptor")
    private String isEnable;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if ("true".equalsIgnoreCase(isEnable)) {
            String header = request.getHeader("X-BSA-GIPHY");
            if (header == null) {
                throw new HeaderMissedException();
            }
        }

        return super.preHandle(request, response, handler);
    }
}
